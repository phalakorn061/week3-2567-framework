function n() {
    console.log("Callback Function")
}
// Nested function
function fulln(Callback, fname, lname) {
    Callback()
    function getfln() {
        return fname +" "+ lname;
    }
    return getfln()
    
}
const msg = fulln(n,"G", "B");
console.log(msg)
