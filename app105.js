// Pure Function
// Side Effect : c = 10
const c = 10
function add(a , b , c) {
    return a + b + c;
}
const sum = add(1, 6);
console.log("Pure Function : ", sum);1
